<div class="alert <?= ($this->session->flashdata('status') == 'success') ? 'alert-success' : 'alert-danger' ?> alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa <?= ($this->session->flashdata('status') == 'success') ? 'fa-check' : 'fa-ban' ?>"></i> <?= strtoupper($this->session->flashdata('status')) ?>!</h4>
	<?= $this->session->flashdata('message') ?>
</div>