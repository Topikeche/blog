<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
$birthDate = new DateTime($user->birth_date);
$joinDate = new DateTime($user->create_date);
?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<!-- Default box -->
<div class="box">
	<div class="box-body">

		<div class="profile">
			<div class="input-group-btn custom">
				<button type="button" class="btn btn-default btn-lg btn-dot3 dropdown-toggle pull-right" data-toggle="dropdown">
					<span class="fa fa-ellipsis-v"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<li><a href="<?= base_url('dashboard/user/edit/'.$user->id) ?>"><i class="fa fa-edit"></i>&nbsp;Edit</a></li>
					<li data-target="#modal-confirm" data-toggle="modal"><a onclick="deleteLink('<?= base_url('dashboard/user/delete/'.$user->id) ?>', '<?= $user->full_name ?>')"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete</a></li>
					<li class="divider"></li>
					<li class="disabled"><a href="#"><i class="fa fa-print"></i>&nbsp;&nbsp;Print</a></li>
				</ul>
			</div>
			<div class="profile-header text-center">
				<div class="photo-profile">
					<div class="photo-wrap">
						<img src="<?= ($this->session->userdata('photo') != NULL) ? base_url('assets/upload/img/'.$this->session->userdata('photo')) : base_url('assets/dashboard/adminLTE/img/avatar4.png') ?>" class="img-circle" alt="User Image" >
						<button class="btn btn-lg btn-primary photo-button"><i class="fa fa-camera"></i></button>
					</div>
				</div>
				<h3><b><?= strtoupper($user->full_name) ?></b></h3>
				<div class="text-blue bold"><?= $user->the_role ?></div>
				<div><i>Registered at <?= $joinDate->format('j M Y, g:i A') ?></i></div>
			</div>
			<div class="profile-content">
				<div class="nav-tabs-custom full-tabs">
					<ul class="nav nav-tabs two-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab">IDENTITY</a></li>
						<li><a href="#tab_2" data-toggle="tab">POST</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							<div class="form-horizontal container">
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">ID User</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $user->id ?></div></div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">Full Name</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $user->full_name ?></div></div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">Email</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $user->email ?></div></div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">Telephone</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $user->phone ?></div></div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">Sex</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $user->the_sex ?></div></div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">Bith Date</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $birthDate->format('D, j M Y') ?></div></div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 col-md-2 col-lg-2 control-label">Address</label>
									<div class="col-sm-9 col-md-10 col-lg-10"><div class="content-label"><?= $user->address ?></div></div>
								</div>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="tab_2">
							The European languages are members of the same family. Their separate existence is a myth.
							For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
							in their grammar, their pronunciation and their most common words. Everyone realizes why a
							new common language would be desirable: one could refuse to pay expensive translators. To
							achieve this, it would be necessary to have uniform grammar, pronunciation and more common
							words. If several languages coalesce, the grammar of the resulting language is more simple
							and regular than that of the individual languages.
						</div>
						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div>
				<!-- nav-tabs-custom -->
			</div>
		</div>

	</div>
</div>

<!-- delete confirm modal -->
<?php $this->load->view('admin/partial/confirm'); ?>

<script>
function deleteLink(link, name){
	document.getElementById('deleteUser').setAttribute("href", link)
	document.getElementById('nameUser').innerHTML = name.toUpperCase()
}
</script>