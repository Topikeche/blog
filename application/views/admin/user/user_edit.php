<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<?php $birth_date = !empty(set_value('birth_date')) ? set_value('birth_date') : null ?>

<!-- Default box -->
<div class="box">
	<div class="box-body">
		<div class="box-body">
			<form action="<?= base_url('dashboard/user/edit_process/').$user->id ?>" method="post">
				<div class="form-horizontal">
					<div class="form-group <?= form_error('full_name')? 'has-error' : '' ?>">
						<label for="full_name" class="col-sm-2 control-label">Full Name</label>
						<div class="col-sm-10">
							<input type="text" name="full_name" class="form-control" value="<?= !empty(set_value('full_name')) ? set_value('full_name') : $user->full_name  ?>" placeholder="Your legal name from without suffix" />
							<?= form_error('full_name', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group <?= form_error('email')? 'has-error' : '' ?>">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" name="email" value="<?= !empty(set_value('email')) ? set_value('email') : $user->email ?>" class="form-control" placeholder="Your active email" disabled />
							<?= form_error('email', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group <?= form_error('phone')? 'has-error' : '' ?>">
						<label for="phone" class="col-sm-2 control-label">Telephone</label>
						<div class="col-sm-10">
							<input type="text" name="phone" value="<?= !empty(set_value('phone')) ? set_value('phone') : $user->phone ?>" class="form-control" placeholder="Your telephone or mobile number" />
							<?= form_error('phone', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group">					
						<label for="sex" class="col-sm-2 control-label">Sex</label>
						<div class="col-sm-10">
							<select name="sex" class="form-control select2" style="width: 100%;">
								<?php 
								$selectSex = !empty(set_value('sex')) ? set_value('sex') : $user->sex;
								foreach($sex as $single) : 
								?>
								<option value="<?= $single->id ?>" <?= ($single->id == $selectSex) ? 'selected' : '' ?> ><?= $single->type ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
					<div class="form-group <?= form_error('birth_date')? 'has-error' : '' ?>">
						<label for="birth_date" class="col-sm-2 control-label">Birth Date</label>
						<div class="col-sm-10">
							<div class="input-group">
								<input id="datepicker-birth" type="text" name="birth_date" class="form-control" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
							<?= form_error('birth_date', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group <?= form_error('address')? 'has-error' : '' ?>">
						<label for="address" class="col-sm-2 control-label">Address</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="address" rows="3" placeholder="As in your identity card"><?= !empty(set_value('address')) ? set_value('address') : $user->address ?></textarea>
							<?= form_error('address', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="role" class="col-sm-2 control-label">Role</label>
						<div class="col-sm-10">
							<select name="role" class="form-control select2" style="width: 100%;">
								<?php 
								$selectRole = !empty(set_value('role')) ? set_value('role') : $user->role;
								foreach($role as $single) : 
								?>
								<option value="<?= $single->id ?>"  <?= ($single->id == $selectRole) ? 'selected' : '' ?> ><?= $single->type ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;&nbsp;Save</button>
			</form>
		</div>
	</div>
</div>


<script src="<?= base_url('assets/dashboard/jquery/jquery.min.js') ?>"></script>
<script>
	let birth_date = <?php echo json_encode($birth_date) ?>;
	$(function () {
		$('#datepicker-birth').datepicker({
			autoclose: true,
			startView: 'decade',
			defaultViewDate: { year: 1980 },
			format: 'yyyy-mm-dd'
		})
		if(birth_date != null) $('#datepicker-birth').datepicker("setDate", new Date(birth_date))
		else $('#datepicker-birth').datepicker("setDate", new Date(<?= json_encode($user->birth_date) ?>))
	});
</script>