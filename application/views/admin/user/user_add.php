<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Check flashdata -->
<?php if (!empty($this->session->flashdata())): $this->load->view('admin/partial/alert'); endif; ?>

<?php $birth_date = !empty(set_value('birth_date')) ? set_value('birth_date') : null ?>

<!-- Default box -->
<div class="box">
	<div class="box-body">
		<div class="box-body">
			<form action="<?= base_url('dashboard/user/add_process') ?>" method="post">
				<div class="form-horizontal">
					<div class="form-group <?= form_error('full_name')? 'has-error' : '' ?>">
						<label for="full_name" class="col-sm-2 control-label">Full Name</label>
						<div class="col-sm-10">
							<input type="text" name="full_name" class="form-control" value="<?= set_value('full_name') ?>" placeholder="Your legal name from without suffix" />
							<?= form_error('full_name', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group <?= form_error('email')? 'has-error' : '' ?>">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" name="email" value="<?= set_value('email') ?>" class="form-control" placeholder="Your active email" />
							<?= form_error('email', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group <?= form_error('phone')? 'has-error' : '' ?>">
						<label for="phone" class="col-sm-2 control-label">Telephone</label>
						<div class="col-sm-10">
							<input type="text" name="phone" value="<?= set_value('phone') ?>" class="form-control" placeholder="Your telephone or mobile number" />
							<?= form_error('phone', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group">					
						<label for="sex" class="col-sm-2 control-label">Sex</label>
						<div class="col-sm-10">
							<select name="sex" class="form-control select2" style="width: 100%;">
							<?php foreach($sex as $single) : ?>
								<option value="<?= $single->id ?>" <?= ($single->id == set_value('sex')) ? 'selected' : '' ?> ><?= $single->type ?></option>
							<?php endforeach ?>
							</select>
						</div>
					</div>
					<div class="form-group <?= form_error('birth_date')? 'has-error' : '' ?>">
						<label for="birth_date" class="col-sm-2 control-label">Birth Date</label>
						<div class="col-sm-10">
							<div class="input-group">
								<input id="datepicker-birth" type="text" name="birth_date" class="form-control" readonly />
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
							<?= form_error('birth_date', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group <?= form_error('address')? 'has-error' : '' ?>">
						<label for="address" class="col-sm-2 control-label">Address</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="address" rows="3" placeholder="As in your identity card"><?= set_value('address') ?></textarea>
							<?= form_error('address', '<small class="text-red">', '</small>'); ?>
						</div>
					</div>
					<div class="form-group">
						<label for="role" class="col-sm-2 control-label">Role</label>
						<div class="col-sm-10">
							<select name="role" class="form-control select2" style="width: 100%;">
							<?php foreach($role as $single) : ?>
								<option value="<?= $single->id ?>"  <?= ($single->id == set_value('role')) ? 'selected' : '' ?> ><?= $single->type ?></option>
							<?php endforeach ?>
							</select>
						</div>
					</div>
					<div class="form-group <?= form_error('password')? 'has-error' : '' ?>">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-6">
									<input type="password" name="password" value="<?= set_value('password') ?>" class="form-control pass-input" placeholder="Type your password" />
									<?= form_error('password', '<small class="text-red">', '</small>'); ?>
								</div>
								<div class="col-sm-6 <?= form_error('password_confirm')? 'has-error' : '' ?>">
									<div id="confirm-error" class="input-group">
										<input type="password" name="password_confirm" value="<?= set_value('password_confirm') ?>" class="form-control pass-input" placeholder="Re:confirm your password" />
										<span class="btn input-group-addon" onclick="tooglePass()"><i id="pass-icon" class="fa fa-eye"></i></span>
									</div>
									<?= form_error('password_confirm', '<small class="text-red">', '</small>'); ?>
									<!-- <small id="confirm-error-msg" class="hidden">Password doesn't match</small> -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;&nbsp;Create</button>
			</form>
		</div>
	</div>
</div>


<script src="<?= base_url('assets/dashboard/jquery/jquery.min.js') ?>"></script>
<script>
	let elmType = document.getElementsByClassName('pass-input')
	function tooglePass() {
		if(elmType[0].type === 'password'){
			elmType[0].type = "text"
			elmType[1].type = "text"
			document.getElementById('pass-icon').className = "fa fa-eye-slash"
		} else {
			elmType[0].type = "password"
			elmType[1].type = "password"
			document.getElementById('pass-icon').className = "fa fa-eye"
		}
	}

	let tmpPassword = ''
	let confirm = ''
	function getPassword() {
		tmpPassword = document.getElementsByClassName('pass-input')[0].value
	}
	function matchPassword(event) {
		confirm = event.target.value
		console.log(event, confirm)
		if(confirm != tmpPassword) {
			document.getElementById('confirm-error').classList.add('has-error')
			document.getElementById('confirm-error-msg').className = 'text-red'
		} else {
			document.getElementById('confirm-error').classList.remove('has-error')
			document.getElementById('confirm-error-msg').className = 'text-red hidden'
		}
	}

	let birth_date = <?php echo json_encode($birth_date) ?>;
	$(function () {
		$('#datepicker-birth').datepicker({
			autoclose: true,
			startView: 'decade',
			defaultViewDate: { year: 1980 },
			format: 'yyyy-mm-dd'
			// format: {
			// 	toDisplay: function (date, format, language) {
			// 			var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
      //       var d = new Date(date);
      //       return d.toLocaleDateString('id-ID', options);
      //   },
      //   toValue: function (date, format, language) {
      //       var d = new Date(date);
      //       return new Date(d);
      //   }
			// }
		})
		if(birth_date != null) $('#datepicker-birth').datepicker("setDate", new Date(birth_date));
	});
</script>