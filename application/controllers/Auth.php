<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
	}

	public function index() {
		redirect('auth/signin');
	}

	public function signin() {
		if($this->utils->isLogin()) : redirect('dashboard'); endif;
		$this->load->view('signin');
	}
	public function signin_process() {
		$userM = $this->User_model;
		$authData = $this->User_model->authData();
		$validation = $this->form_validation->set_rules($userM->rulesForSignin());
		if($validation->run()) {
			if($authData != null){
				$names = explode(" ", $authData->full_name);
				$this->session->set_userdata([
					'id' => $authData->id,
					'full_name' => $authData->full_name, 
					'short_name' => $names[0]." ".$names[1],
					'email' => $authData->email, 
					'photo' => $authData->photo,
					'role' => $authData->role, 
					'create_date' => $authData->create_date,
					'isLogin' => TRUE
				]);
				redirect('dashboard/overview');
			} else{
				$this->session->set_flashdata(['status' => 'failed', 'message' => 'Email & password doesn\'t match']);
				$this->load->view('signin');
			}
		} else{
			$this->load->view('signin');
		}
		// $data['user2'] = $this->User_model->getById(1);
	}

	public function signout() {
		$this->session->sess_destroy();
		redirect('auth/signin');
	}

}