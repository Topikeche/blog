<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->utils->isLogin()){ redirect('auth/signin'); }
	}

	public function index() {
		$data = array();
		$this->template->set('title', 'Overview');
		$this->template->load('layout', 'contents' , 'admin/overview', $data);
	}
  
}