<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utils {

	protected $CI;

	function __construct() {
		$this->CI =& get_instance();
	}

	public function isLogin(){
		if($this->CI->session->userdata('isLogin')){
			return TRUE;
		} else{
			return FALSE;
		}
	}

	public function isAdmin(){
		if($this->CI->session->userdata('isLogin') && $this->CI->session->userdata('role') == '1'){
			return TRUE;
		} else{
			return FALSE;
		}
	}

	public function isWriter(){
		if($this->CI->session->userdata('isLogin') && $this->CI->session->userdata('role') == '2'){
			return TRUE;
		} else{
			return FALSE;
		}
	}

}